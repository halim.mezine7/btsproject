-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  lun. 04 juin 2018 à 15:52
-- Version du serveur :  10.1.30-MariaDB
-- Version de PHP :  7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `e4`
--

DELIMITER $$
--
-- Procédures
--
CREATE DEFINER=`'root'`@`'localhost'` PROCEDURE `agence_tech` (IN `agence` INT)  NO SQL
SELECT *
FROM technicien
Where agence = Numero_Agence$$

CREATE DEFINER=`'root'`@`'localhost'` PROCEDURE `Intervention_DAte` (IN `Date` INT)  NO SQL
SELECT * 
FROM Intervention
where date = Date_Visite$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `agence`
--

CREATE TABLE `agence` (
  `numAgence` int(11) NOT NULL,
  `nomAgence` varchar(25) DEFAULT NULL,
  `adresseAgence` varchar(25) DEFAULT NULL,
  `telephoneAgence` int(11) DEFAULT NULL,
  `faxAgence` int(25) DEFAULT NULL,
  `codeRegion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `agence`
--

INSERT INTO `agence` (`numAgence`, `nomAgence`, `adresseAgence`, `telephoneAgence`, `faxAgence`, `codeRegion`) VALUES
(1, 'TestAgence', 'TestAdresse', 808080808, 4757575, '3'),
(2, 'CashCashLille', '98 rue de tesvirer', 606060606, 7944587, '1'),
(3, 'CashCashCroix', '97 rue de tesencorevirer', 707070704, 575757, '1');

-- --------------------------------------------------------

--
-- Structure de la table `assistanttelephonique`
--

CREATE TABLE `assistanttelephonique` (
  `matricule` int(11) NOT NULL,
  `codeRegion` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `assistanttelephonique`
--

INSERT INTO `assistanttelephonique` (`matricule`, `codeRegion`) VALUES
(5, 1),
(1, 2),
(3, 4);

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `numClient` int(11) NOT NULL,
  `raisonSociale` varchar(25) DEFAULT NULL,
  `siren` varchar(25) DEFAULT NULL,
  `codeApe` varchar(25) DEFAULT NULL,
  `adresse` varchar(25) DEFAULT NULL,
  `telephoneClient` int(11) DEFAULT NULL,
  `faxClient` varchar(25) DEFAULT NULL,
  `email` varchar(25) DEFAULT NULL,
  `dureeDeplacement` double DEFAULT NULL,
  `distanceKm` double DEFAULT NULL,
  `numAgence` int(11) DEFAULT NULL,
  `numContrat` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`numClient`, `raisonSociale`, `siren`, `codeApe`, `adresse`, `telephoneClient`, `faxClient`, `email`, `dureeDeplacement`, `distanceKm`, `numAgence`, `numContrat`) VALUES
(1, 'test', 'TestSiren', '1234', 'TestAdresse', 8757857, 'TestFax', 'halim_minato@hotmail.fr', 2, 3898, 1, 1),
(157856, 'MKTN Group', '184844F1', '47887D', '8 avenue de la créativité', 67214874, '05448888', 'halim12mai@gmail.com', 20, 25, 2, 4);

-- --------------------------------------------------------

--
-- Structure de la table `connexion`
--

CREATE TABLE `connexion` (
  `idConnexion` int(50) NOT NULL,
  `login` varchar(50) NOT NULL,
  `mdp` varchar(50) NOT NULL,
  `matricule` int(11) NOT NULL,
  `id_droit` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `connexion`
--

INSERT INTO `connexion` (`idConnexion`, `login`, `mdp`, `matricule`, `id_droit`) VALUES
(1, 'admin', 'admin', 5, 1),
(2, 'technicien', 'technicien', 3, 2),
(3, 'assistant', 'assistant', 2, 3);

-- --------------------------------------------------------

--
-- Structure de la table `contratmaintenance`
--

CREATE TABLE `contratmaintenance` (
  `numContrat` int(11) NOT NULL,
  `dateSignature` date DEFAULT NULL,
  `dateEcheance` date DEFAULT NULL,
  `refTypeContrat` varchar(25) DEFAULT NULL,
  `numClient` int(11) DEFAULT NULL,
  `RelanceEmail` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `contratmaintenance`
--

INSERT INTO `contratmaintenance` (`numContrat`, `dateSignature`, `dateEcheance`, `refTypeContrat`, `numClient`, `RelanceEmail`) VALUES
(1, '2017-11-01', '2018-01-31', 'TestTypeContrat', 1, 1),
(4, '2017-11-12', '2018-05-15', 'Grosse Caisse', 157856, 1);

-- --------------------------------------------------------

--
-- Structure de la table `controler`
--

CREATE TABLE `controler` (
  `tempsPasse` double NOT NULL,
  `commentaire` varchar(25) DEFAULT NULL,
  `numSerie` int(11) NOT NULL,
  `NumIntervention` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déclencheurs `controler`
--
DELIMITER $$
CREATE TRIGGER `addNbCommentaire` AFTER INSERT ON `controler` FOR EACH ROW UPDATE nbcommentaire SET `nbcommentaire`= `nbcommentaire`+1
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `droits`
--

CREATE TABLE `droits` (
  `id_droit` int(11) NOT NULL,
  `libelle_droit` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `droits`
--

INSERT INTO `droits` (`id_droit`, `libelle_droit`) VALUES
(1, 'admin'),
(2, 'technicien'),
(3, 'assistant');

-- --------------------------------------------------------

--
-- Structure de la table `employe`
--

CREATE TABLE `employe` (
  `matricule` int(11) NOT NULL,
  `nomEmploye` varchar(25) DEFAULT NULL,
  `prenomEmploye` varchar(25) DEFAULT NULL,
  `adresseEmploye` varchar(25) DEFAULT NULL,
  `dateEmbauche` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `employe`
--

INSERT INTO `employe` (`matricule`, `nomEmploye`, `prenomEmploye`, `adresseEmploye`, `dateEmbauche`) VALUES
(1, 'nom', 'prenom', 'adresse', '2017-06-06'),
(2, 'Mezine', 'Halim', 'lol', '2017-03-21'),
(3, 'Bandarra', 'Antoine', 'virer', '2017-05-16'),
(4, 'Wahad', 'Amaguiz', 'haha', '2017-06-13'),
(5, 'kkkkkk', 'kkkkkkkk', 'avenue de l\'avenue', '2017-11-29');

-- --------------------------------------------------------

--
-- Structure de la table `erreur`
--

CREATE TABLE `erreur` (
  `id` tinyint(11) NOT NULL,
  `erreur` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `famille`
--

CREATE TABLE `famille` (
  `codeFamille` varchar(2) NOT NULL,
  `libelleFamille` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `famille`
--

INSERT INTO `famille` (`codeFamille`, `libelleFamille`) VALUES
('CB', 'lecteur de cartes'),
('CP', 'caisse avec processeur'),
('LO', 'lecteur optique'),
('MO', 'modem'),
('PC', 'petite caisse'),
('SE', 'serveur'),
('SP', 'Smartphone'),
('SS', 'souris sans fil');

-- --------------------------------------------------------

--
-- Structure de la table `intervention`
--

CREATE TABLE `intervention` (
  `NumIntervention` int(11) NOT NULL,
  `dateVisite` date DEFAULT NULL,
  `heureVisite` double DEFAULT NULL,
  `validation` tinyint(1) NOT NULL,
  `numClient` int(11) DEFAULT NULL,
  `telMobile` int(11) DEFAULT NULL,
  `matricule` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `intervention`
--

INSERT INTO `intervention` (`NumIntervention`, `dateVisite`, `heureVisite`, `validation`, `numClient`, `telMobile`, `matricule`) VALUES
(0, '2018-06-14', 10, 0, 157856, 2532535, 2),
(1, '2018-06-14', 10, 1, 1, 4654, 4),
(2, '2017-11-23', 20, 1, 1, 6060606, 4),
(3, '2018-05-10', 22, 1, 1, 6060606, 4),
(4, '2018-05-09', 10, 1, 1, 4654, 4);

-- --------------------------------------------------------

--
-- Structure de la table `materiel`
--

CREATE TABLE `materiel` (
  `numSerie` int(11) NOT NULL,
  `dateDeVente` date DEFAULT NULL,
  `dateInstallation` date DEFAULT NULL,
  `prixVente` float NOT NULL,
  `emplacement` varchar(20) DEFAULT NULL,
  `referenceInterne` varchar(25) DEFAULT NULL,
  `numContrat` int(11) DEFAULT NULL,
  `numClient` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `materiel`
--

INSERT INTO `materiel` (`numSerie`, `dateDeVente`, `dateInstallation`, `prixVente`, `emplacement`, `referenceInterne`, `numContrat`, `numClient`) VALUES
(1, '2017-11-01', '2017-11-07', 100, 'Caisse principale', 'A 506', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `nbcommentaire`
--

CREATE TABLE `nbcommentaire` (
  `nbCommentaire` int(111) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `nbcommentaire`
--

INSERT INTO `nbcommentaire` (`nbCommentaire`) VALUES
(2);

-- --------------------------------------------------------

--
-- Structure de la table `region`
--

CREATE TABLE `region` (
  `codeRegion` int(11) NOT NULL,
  `nomRegion` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `region`
--

INSERT INTO `region` (`codeRegion`, `nomRegion`) VALUES
(1, 'Hauts-de-France'),
(2, 'Normandie'),
(3, 'Ile-de-France'),
(4, 'Grand-Est'),
(5, 'Bretagne');

-- --------------------------------------------------------

--
-- Structure de la table `technicien`
--

CREATE TABLE `technicien` (
  `telMobile` int(50) NOT NULL,
  `qualification` varchar(50) NOT NULL,
  `dateObtention` date NOT NULL,
  `matricule` int(50) NOT NULL,
  `numAgence` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `technicien`
--

INSERT INTO `technicien` (`telMobile`, `qualification`, `dateObtention`, `matricule`, `numAgence`) VALUES
(4654, 'Confirmé', '2017-11-30', 4, 1),
(2532535, 'Pro', '2018-06-11', 2, 3),
(6060606, 'TestQualification', '2017-11-01', 2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `typecontrat`
--

CREATE TABLE `typecontrat` (
  `refTypeContrat` varchar(25) NOT NULL,
  `delaiIntervention` double DEFAULT NULL,
  `tauxApplicable` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `typecontrat`
--

INSERT INTO `typecontrat` (`refTypeContrat`, `delaiIntervention`, `tauxApplicable`) VALUES
('Grosse Caisse', 877588, 0.5),
('Petite Caisse', 698487, 0.1),
('TestTypeContrat', 11, 11);

-- --------------------------------------------------------

--
-- Structure de la table `typemateriel`
--

CREATE TABLE `typemateriel` (
  `referenceInterne` varchar(25) NOT NULL,
  `libelleTypeMateriel` varchar(100) DEFAULT NULL,
  `codeFamille` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `typemateriel`
--

INSERT INTO `typemateriel` (`referenceInterne`, `libelleTypeMateriel`, `codeFamille`) VALUES
('A 506', 'souris sans fil logitech', 'SS'),
('B788', 'Serveur', 'SE'),
('H781', 'Ordinateur', 'CP'),
('T784', 'Iphone X', 'SP'),
('U7884', 'Petite Caisse', 'PC');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `agence`
--
ALTER TABLE `agence`
  ADD PRIMARY KEY (`numAgence`),
  ADD KEY `codeRegion` (`codeRegion`);

--
-- Index pour la table `assistanttelephonique`
--
ALTER TABLE `assistanttelephonique`
  ADD PRIMARY KEY (`matricule`),
  ADD KEY `FK_assistantTelephonique_codeRegion` (`codeRegion`);

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`numClient`),
  ADD KEY `FK_Client_numAgence` (`numAgence`),
  ADD KEY `numContrat` (`numContrat`);

--
-- Index pour la table `connexion`
--
ALTER TABLE `connexion`
  ADD PRIMARY KEY (`idConnexion`),
  ADD KEY `matricule` (`matricule`),
  ADD KEY `id_droit` (`id_droit`);

--
-- Index pour la table `contratmaintenance`
--
ALTER TABLE `contratmaintenance`
  ADD PRIMARY KEY (`numContrat`),
  ADD KEY `FK_ContratMaintenance_refTypeContrat` (`refTypeContrat`),
  ADD KEY `FK_ContratMaintenance_numClient` (`numClient`);

--
-- Index pour la table `controler`
--
ALTER TABLE `controler`
  ADD KEY `NumIntervention` (`NumIntervention`),
  ADD KEY `numSerie` (`numSerie`);

--
-- Index pour la table `droits`
--
ALTER TABLE `droits`
  ADD PRIMARY KEY (`id_droit`);

--
-- Index pour la table `employe`
--
ALTER TABLE `employe`
  ADD PRIMARY KEY (`matricule`);

--
-- Index pour la table `erreur`
--
ALTER TABLE `erreur`
  ADD PRIMARY KEY (`id`),
  ADD KEY `erreur` (`erreur`);

--
-- Index pour la table `famille`
--
ALTER TABLE `famille`
  ADD PRIMARY KEY (`codeFamille`);

--
-- Index pour la table `intervention`
--
ALTER TABLE `intervention`
  ADD PRIMARY KEY (`NumIntervention`),
  ADD KEY `FK_Intervention_numClient` (`numClient`),
  ADD KEY `FK_Intervention_telMobile` (`telMobile`),
  ADD KEY `FK_Intervention_matricule` (`matricule`);

--
-- Index pour la table `materiel`
--
ALTER TABLE `materiel`
  ADD PRIMARY KEY (`numSerie`),
  ADD KEY `FK_Materiel_referenceInterne` (`referenceInterne`),
  ADD KEY `FK_Materiel_numContrat` (`numContrat`),
  ADD KEY `FK_Materiel_numClient` (`numClient`);

--
-- Index pour la table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`codeRegion`);

--
-- Index pour la table `technicien`
--
ALTER TABLE `technicien`
  ADD PRIMARY KEY (`telMobile`),
  ADD KEY `numAgence` (`numAgence`),
  ADD KEY `matricule` (`matricule`),
  ADD KEY `numAgence_2` (`numAgence`);

--
-- Index pour la table `typecontrat`
--
ALTER TABLE `typecontrat`
  ADD PRIMARY KEY (`refTypeContrat`);

--
-- Index pour la table `typemateriel`
--
ALTER TABLE `typemateriel`
  ADD PRIMARY KEY (`referenceInterne`),
  ADD KEY `FK_TypeMateriel_codeFamille` (`codeFamille`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `connexion`
--
ALTER TABLE `connexion`
  MODIFY `idConnexion` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `droits`
--
ALTER TABLE `droits`
  MODIFY `id_droit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `assistanttelephonique`
--
ALTER TABLE `assistanttelephonique`
  ADD CONSTRAINT `FK_assistantTelephonique_codeRegion` FOREIGN KEY (`codeRegion`) REFERENCES `region` (`codeRegion`),
  ADD CONSTRAINT `FK_assistantTelephonique_matricule` FOREIGN KEY (`matricule`) REFERENCES `employe` (`matricule`);

--
-- Contraintes pour la table `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `FK_Client_numAgence` FOREIGN KEY (`numAgence`) REFERENCES `agence` (`numAgence`);

--
-- Contraintes pour la table `contratmaintenance`
--
ALTER TABLE `contratmaintenance`
  ADD CONSTRAINT `FK_ContratMaintenance_numClient` FOREIGN KEY (`numClient`) REFERENCES `client` (`numClient`),
  ADD CONSTRAINT `FK_ContratMaintenance_refTypeContrat` FOREIGN KEY (`refTypeContrat`) REFERENCES `typecontrat` (`refTypeContrat`);

--
-- Contraintes pour la table `intervention`
--
ALTER TABLE `intervention`
  ADD CONSTRAINT `FK_Intervention_matricule` FOREIGN KEY (`matricule`) REFERENCES `employe` (`matricule`),
  ADD CONSTRAINT `FK_Intervention_numClient` FOREIGN KEY (`numClient`) REFERENCES `client` (`numClient`),
  ADD CONSTRAINT `FK_Intervention_telMobile` FOREIGN KEY (`telMobile`) REFERENCES `technicien` (`telMobile`);

--
-- Contraintes pour la table `materiel`
--
ALTER TABLE `materiel`
  ADD CONSTRAINT `FK_Materiel_numClient` FOREIGN KEY (`numClient`) REFERENCES `client` (`numClient`),
  ADD CONSTRAINT `FK_Materiel_numContrat` FOREIGN KEY (`numContrat`) REFERENCES `contratmaintenance` (`numContrat`),
  ADD CONSTRAINT `FK_Materiel_referenceInterne` FOREIGN KEY (`referenceInterne`) REFERENCES `typemateriel` (`referenceInterne`);

--
-- Contraintes pour la table `technicien`
--
ALTER TABLE `technicien`
  ADD CONSTRAINT `technicien_ibfk_1` FOREIGN KEY (`matricule`) REFERENCES `employe` (`matricule`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
